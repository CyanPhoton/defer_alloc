pub mod defer_alloc_arc;
pub mod defer_alloc_rc;

pub use defer_alloc_arc::DeferAllocArc;
pub use defer_alloc_rc::DeferAllocRc;