use std::ops::{Deref, DerefMut};
use std::rc::Rc;

pub struct DeferAllocRc<T: Sized> {
    ty: DeferAllocRcType<T>
}

enum DeferAllocRcType<T: Sized> {
    Original(parking_lot::RwLock<DeferAllocRcInternal<T>>),
    Cloned(Rc<T>)
}

enum DeferAllocRcInternal<T: Sized> {
    Rc(Rc<T>),
    Stack(T),
}

impl<T: Sized> DeferAllocRc<T> {
    pub fn new(value: T) -> DeferAllocRc<T> {
        DeferAllocRc {
            ty: DeferAllocRcType::Original(parking_lot::RwLock::new(DeferAllocRcInternal::Stack(value)))
        }
    }

    // Returns if it's still on the stack, aka has never been cloned
    // If you own the DeferAllocRc then this will remain true so long as you don't clone it (since no other place could have ref to it)
    pub fn is_stack(&self) -> bool {
        if let DeferAllocRcType::Original(o) = &self.ty {
            if let DeferAllocRcInternal::Stack(_) = o.read().deref() {
                return true;
            }
        }

        false
    }

    pub fn with<U, F: FnOnce(&T) -> U>(&self, f: F) -> U{
        match &self.ty {
            DeferAllocRcType::Original(lock) => {
                match lock.read().deref() {
                    DeferAllocRcInternal::Rc(rc) => {
                        f(rc.deref())
                    }
                    DeferAllocRcInternal::Stack(stack) => {
                        f(stack)
                    }
                }
            }
            DeferAllocRcType::Cloned(rc) => {
                f(rc.deref())
            }
        }
    }
}

impl<T: Sized> Clone for DeferAllocRc<T> {
    fn clone(&self) -> Self {
        DeferAllocRc {
            ty: match &self.ty {
                DeferAllocRcType::Original(internal) => {
                    let read = internal.read();
                    if let DeferAllocRcInternal::Rc(rc) = read.deref() {
                        DeferAllocRcType::Cloned(rc.clone())
                    } else {
                        core::mem::drop(read); // Drop read to we can acquire write lock
                        // Acquire write lock on internal enum
                        let mut internal = internal.write();
                        match internal.deref() {
                            DeferAllocRcInternal::Rc(rc) => {
                                // In case another thread acquire write first, blocked this one, and did conversion
                                DeferAllocRcType::Cloned(rc.clone())
                            }
                            DeferAllocRcInternal::Stack(stack) => {
                                // We are first to acquire write lock

                                // Read value from stack, safe since from ptr and we will "forget" original since generally non Copy, and might have Drop
                                let value = unsafe { std::ptr::read(stack) };
                                let rc = Rc::new(value);

                                // Override the internal enum, RcLock guarantees we have exclusive write and it's aligned
                                unsafe { std::ptr::write(internal.deref_mut(), DeferAllocRcInternal::Rc(rc.clone())); }

                                DeferAllocRcType::Cloned(rc)
                            }
                        }
                    }
                }
                DeferAllocRcType::Cloned(rc) => {
                    DeferAllocRcType::Cloned(rc.clone())
                }
            }
        }
    }
}