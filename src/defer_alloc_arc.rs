use std::ops::{Deref, DerefMut};
use std::sync::Arc;

pub struct DeferAllocArc<T: Sized> {
    ty: DeferAllocArcType<T>
}

enum DeferAllocArcType<T: Sized> {
    Original(parking_lot::RwLock<DeferAllocArcInternal<T>>),
    Cloned(Arc<T>)
}

enum DeferAllocArcInternal<T: Sized> {
    Arc(Arc<T>),
    Stack(T),
}

impl<T: Sized> DeferAllocArc<T> {
    pub fn new(value: T) -> DeferAllocArc<T> {
        DeferAllocArc {
            ty: DeferAllocArcType::Original(parking_lot::RwLock::new(DeferAllocArcInternal::Stack(value)))
        }
    }

    // Returns if it's still on the stack, aka has never been cloned
    // If you own the DeferAllocArc then this will remain true so long as you don't clone it (since no other place could have ref to it)
    pub fn is_stack(&self) -> bool {
        if let DeferAllocArcType::Original(o) = &self.ty {
            if let DeferAllocArcInternal::Stack(_) = o.read().deref() {
                return true;
            }
        }

        false
    }

    pub fn with<U, F: FnOnce(&T) -> U>(&self, f: F) -> U {
        match &self.ty {
            DeferAllocArcType::Original(lock) => {
                match lock.read().deref() {
                    DeferAllocArcInternal::Arc(arc) => {
                        f(arc.deref())
                    }
                    DeferAllocArcInternal::Stack(stack) => {
                        f(stack)
                    }
                }
            }
            DeferAllocArcType::Cloned(arc) => {
                f(arc.deref())
            }
        }
    }
}

impl<T: Sized> Clone for DeferAllocArc<T> {
    fn clone(&self) -> Self {
        DeferAllocArc {
            ty: match &self.ty {
                DeferAllocArcType::Original(internal) => {
                    let read = internal.read();
                    if let DeferAllocArcInternal::Arc(arc) = read.deref() {
                        DeferAllocArcType::Cloned(arc.clone())
                    } else {
                        core::mem::drop(read); // Drop read to we can acquire write lock
                        // Acquire write lock on internal enum
                        let mut internal = internal.write();
                        match internal.deref() {
                            DeferAllocArcInternal::Arc(arc) => {
                                // In case another thread acquire write first, blocked this one, and did conversion
                                DeferAllocArcType::Cloned(arc.clone())
                            }
                            DeferAllocArcInternal::Stack(stack) => {
                                // We are first to acquire write lock

                                // Read value from stack, safe since from ptr and we will "forget" original since generally non Copy, and might have Drop
                                let value = unsafe { std::ptr::read(stack) };
                                let arc = Arc::new(value);

                                // Override the internal enum, RcLock guarantees we have exclusive write and it's aligned
                                unsafe { std::ptr::write(internal.deref_mut(), DeferAllocArcInternal::Arc(arc.clone())); }

                                DeferAllocArcType::Cloned(arc)
                            }
                        }
                    }
                }
                DeferAllocArcType::Cloned(arc) => {
                    DeferAllocArcType::Cloned(arc.clone())
                }
            }
        }
    }
}